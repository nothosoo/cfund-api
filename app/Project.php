<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    

    public function category(){
        return $this->belongsTo('App\Category');
    }

    public function posts(){
        return $this->hasMany('App\Post');
    }

    public function user(){
        return $this->belongsTo('App\User');
    }

    public function donations(){
        return $this->hasMany('App\Donation');
    }

    public function store($project, $id){
        $this->category_id = $project->category_id;
        $this->is_fixed_goal = $project->is_fixed_goal;
        $this->goal_amount = $project->goal_amount;
        $this->start_date = $project->start_date;
        $this->end_date = $project->end_date;
        $this->name = $project->name;
        $this->description = $project->description;
        $this->overview = $project->overview;
        $this->story = $project->story;
        $this->thumbnail_img = $project->thumbnail_img;
        $this->user_id = $id;
        $this->save();
        return $this;
    }

    public function updateProject($project, $id){
        $this->category_id = $project->category_id;
        $this->is_fixed_goal = $project->is_fixed_goal;
        $this->goal_amount = $project->goal_amount;
        $this->start_date = $project->start_date;
        $this->end_date = $project->end_date;
        $this->name = $project->name;
        $this->description = $project->description;
        $this->overview = $project->overview;
        $this->story = $project->story;
        $this->thumbnail_img = $project->thumbnail_img;
        $this->user_id = $id;
        $this->save();
        return $this;
    }
}
