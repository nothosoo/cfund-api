<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Donation extends Model
{
    public function project(){
        return $this->belongsTo('App\Project');
    }

    public function store($id, $donation){
        $this->project_id = $id;
        $this->is_anonymous = $donation->isAnonymous;
        $this->name = $donation->name;
        $this->email = $donation->email;
        $this->comment = $donation->comment;
        $this->amount = $donation->amount;
        $this->save();
        return $this;
    }
}
