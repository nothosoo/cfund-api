<?php

namespace App\Http\Controllers\Settings;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProfileController extends Controller
{
    /**
     * Update the user's profile information.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $user = $request->user();

        $this->validate($request, [
            'email' => 'required|email|unique:users,email,'.$user->id,
            'user_type_id' => 'required',
            'dob' => 'required',
            'bank_id' => 'required',
            'account_no' => 'required',
            'phone_number' => 'required'
        ]);

        return tap($user)->update($request->only(
            'user_type_id',
            'org_name',
            'last_name',
            'first_name',
            'profile_img',
            'facebook_url',
            'twitter_url',
            'linkedin_url',
            'dob',
            'bank_id',
            'account_no',
            'phone_number',
            'email'
        ));
    }
}
