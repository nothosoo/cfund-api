<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Project;
use App\Http\Resources\Project as ProjectResource;
use App\Http\Resources\Projects as ProjectsResource;
use Auth;

class ProjectController extends Controller{
    
    protected $project;

    public function __construct(Project $project){
        $this->project = $project;
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return new ProjectsResource(Project::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id){
        $request->validate([
            'category_id' => 'required|numeric',
            'is_fixed_goal' => 'required|boolean',
            'goal_amount' => 'required|numeric',
            'start_date' => 'required',
            'end_date' => 'required',
            'name' => 'required',
            'description' => 'required',
            'overview' => 'required',
            'story' => 'required',
            'thumbnail_img' => 'required'
        ]);

        $this->project->store($request, $id);
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return new ProjectResource(Project::findOrFail($id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id){
        if($id == Auth::id()){
            $request->validate([
                'category_id' => 'required|numeric',
                'is_fixed_goal' => 'required|boolean',
                'goal_amount' => 'required|numeric',
                'start_date' => 'required',
                'end_date' => 'required',
                'name' => 'required',
                'description' => 'required',
                'overview' => 'required',
                'story' => 'required',
                'thumbnail_img' => 'required'
            ]);
            $this->project = Project::find($request->id);
            $this->project->updateProject($request, $id);
        }else{
            abort(500);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->project = Project::find($id);
        $this->project->delete();
        return response()->json(['success' => true]);
    }

    public function userProjects($id){
        return new ProjectsResource(Project::where('user_id', $id)->get());
    }

    public function latest(){
        return new ProjectsResource(Project::orderBy('created_at', 'desc')->take(8)->get());
    }

    public function finished(){
        return new ProjectsResource(Project::orderBy('goal_amount', 'desc')->take(8)->get());
    }
}
