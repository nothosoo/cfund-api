<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\Http\Resources\Categories as CategoriesResource;

class CategoryController extends Controller
{

    protected $category;

    public function __construct(Category $category){
        $this->category = $category;
    }

    public function index(){
        return new CategoriesResource(Category::all());
    }
}
