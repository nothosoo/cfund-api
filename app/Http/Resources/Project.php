<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Project extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'category_id' => $this->category_id,
            'created_at' => $this->created_at,
            'description' => $this->description,
            'description_en' => $this->description_en,
            'end_date' => $this->end_date,
            'goal_amount' => number_format($this->goal_amount),
            'id' => $this->id,
            'is_fixed_goal' => $this->is_fixed_goal,
            'name' => $this->name,
            'name_en' => $this->name_en,
            'overview' => $this->overview,
            'overview_en' => $this->overview_en,
            'start_date' => $this->start_date,
            'story' => $this->story,
            'story_en' => $this->story_en,
            'thumbnail_img' => $this->thumbnail_img,
            'updated_at' => $this->updated,
            'relations' => [
                'category' => $this->category,
                'user' => $this->user,
                'donations' => Donation::collection($this->donations),
                'total_donation' => number_format($this->donations->sum('amount')),
                'posts' => $this->posts
            ],
        ];
    }
}
