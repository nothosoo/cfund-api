<?php

use Illuminate\Database\Seeder;
use App\Donation;

class DonationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){
        $faker = Faker\Factory::create();
        for ($i=1; $i < 500; $i++) { 
            $donation = new Donation();
            $donation->project_id = rand(1, 99);
            $donation->amount = rand(100000, 20000000);
            if($i%5 === 0){
                $donation->is_anonymous = true;
                $donation->save();
            }else{
                $donation->is_anonymous = false;
                $donation->name = $faker->name;
                $donation->email = $faker->word.'@gmail.com';
                $donation->comment = $faker->text(100);
                $donation->save();
            }
        }

    }   
}
