<?php

use Illuminate\Database\Seeder;
use App\Project;

class ProjectsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        for ($i=1; $i < 100; $i++) { 
            $project = new Project();
            $project->category_id = rand(1, 28);
            $project->is_fixed_goal = ($i % 2 == 0) ? true : false;
            $project->goal_amount = $i * 1000000;
            $project->start_date = "2019-".rand(1, 12)."-".rand(1, 28);
            $project->end_date = "2019-".rand(1, 12)."-".rand(1, 28);
            $project->name = $faker->company;
            $project->name_en = $faker->company;
            $project->description = $faker->realText(200, 1);
            $project->description_en = $faker->realText(200, 1);
            $project->overview = $faker->realText(300, 1);
            $project->overview_en = $faker->realText(300, 1);
            $project->story = $faker->realText(1000, 1);
            $project->story_en = $faker->realText(1000, 1);
            $project->thumbnail_img = "https://picsum.photos/1280/720?image=".$i;
            $project->user_id = 1;
            $project->save();
        }
    }
}
