<?php

use Illuminate\Database\Seeder;
use App\Category;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        //1 - TECH & INNOVATION
        //2 - CREATIVE WORKS
        //3 - COMMUNITY PROJECTS
        $categories = [
            [
                ['Audio', 'Дуу'],
                ['Camera Gear', 'Камер'],
                ['Education', 'Боловсрол'],
                ['Energy & Green Tech', 'Эрчим хүч & Сэргээгдэх эрчим хүч'],
                ['Fashion & Wearables', 'Загвар & Хувцас'],
                ['Food & Beverages', 'Хоол & Хүнс'],
                ['Health & Fitness', 'Эрүүл мэнд & Фитнесс'],
                ['Home', 'Гэр ахуй'],
                ['Phones & Accessories', 'Утас & Хэрэгсэл'],
                ['Productivity', 'Бүтээмж'],
                ['Transportation', 'Тээвэр'],
                ['Travel & Outdoors', 'Аялал & Зугаалга']
            ],

            [
                ['Art', 'Дүрслэх урлаг'],
                ['Comics', 'Зурагт ном'],
                ['Dance & Theater', 'Бүжиг & Театр'],
                ['Film', 'Кино'],
                ['Music', 'Дуу'],
                ['Photography', 'Гэрэл зураг'],
                ['Podcasts, Blogs & Vlogs', 'Подкаст, Блог & Влог'],
                ['Tabletop Games', 'Ширээний тоглоом'],
                ['Video Games', 'Видео тоглоом'],
                ['Web Series & TV Shows', 'Веб цуврал & ТВ Шоу'],
                ['Writing & Publishing', 'Ном зохиол & Нийтлэл']
            ],
            
            [
                ['Culture', 'Соёл' ],
                ['Environment', 'Байгаль орчин' ],
                ['Human Rights', 'Хүний эрх' ],
                ['Local Businesses', 'Дотоодын бизнесс' ],
                ['Wellness', 'Эрүүл амьдрал' ]
            ]
        ];

        foreach ($categories as $i => $cat) {
            foreach ($cat as $j => $c) {
                if($i == 0){
                    $ca = new Category();
                    $ca->name = $c[1];
                    $ca->name_en = $c[0];
                    $ca->parent = 1;
                    $ca->save();
                }elseif($i == 1){
                    $ca = new Category();
                    $ca->name = $c[1];
                    $ca->name_en = $c[0];
                    $ca->parent = 2;
                    $ca->save();
                }elseif($i == 2){
                    $ca = new Category();
                    $ca->name = $c[1];
                    $ca->name_en = $c[0];
                    $ca->parent = 3;
                    $ca->save();
                }
            }
        }
    }
}
