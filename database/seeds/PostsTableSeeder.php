<?php

use Illuminate\Database\Seeder;
use App\Post;

class PostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){
        $faker = Faker\Factory::create();
        for ($i=1; $i < 501; $i++) { 
            $post = new Post();
            $post->project_id = rand(1, 99);
            $post->title =  $faker->text(100);
            $post->body = $faker->text(1000);
            $post->save();
        }

    }
}
