<?php

use Illuminate\Database\Seeder;
use App\UserType;

class UserTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $type = new UserType();
        $type->name = "Хувь хүн";
        $type->name_en = "Person";
        $type->description = "Person";
        $type->save();
    
        $type = new UserType();
        $type->name = "Байгууллага";
        $type->name_en = "Person";
        $type->description = "Organization";
        $type->save();
    }
}
