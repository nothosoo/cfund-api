<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = new User();
        $user->user_type_id = 2;
        $user->org_name = "Туршилтын байгууллага";
        $user->org_name_en = "Test organization";
        $user->dob = "2019-01-01";
        $user->bio = "Байгууллагын товч танилцуулга";
        $user->bio_en = "Short bio of organization";
        $user->phone_number = "80500750";
        $user->email = "hosoo.xoc@gmail.com";
        $user->bank_id = 1;
        $user->account_no = "5071262796";
        $user->password = bcrypt('saitama');
        $user->save();
    }
}
