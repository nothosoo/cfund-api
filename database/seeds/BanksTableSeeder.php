<?php

use Illuminate\Database\Seeder;
use App\Bank;

class BanksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $banks = [
            'Хаан банк',
            'Хас банк',
            'Худалдаа хөгжлийн банк',
            'Голомт банк',
            'Төрийн банк',
            'Улаанбаатар хотын банк',
            'Капитал банк',
            'Капитрон банк',
            'Ариг банк',
            'Чингис хаан банк',
            'Үндэсний хөрөнгө оруулалтын банк',
            'Кредит банк',
            'Тээвэр хөгжлийн банк',
            'Богд банк'
        ];

        foreach ($banks as $key => $bank) {
            $b = new Bank();
            $b->name = $bank;
            $b->save();
        }
    }
}
