<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('category_id')->unsigned();
            $table->boolean('is_fixed_goal')->default(false);
            $table->double('goal_amount');
            $table->date('start_date');
            $table->date('end_date');
            $table->string('name');
            $table->string('name_en')->nullable();
            $table->string('description');
            $table->string('description_en')->nullable();
            $table->text('overview');
            $table->text('overview_en')->nullable();
            $table->text('story');
            $table->text('story_en')->nullable();
            $table->string('thumbnail_img');
            $table->integer('user_id')->unsigned();
            $table->timestamps();

            $table->foreign('category_id')->references('id')->on('categories')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projects');
    }
}
