<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_type_id')->unsigned();
            $table->string('org_name', 100)->nullable();
            $table->string('org_name_en', 100)->nullable();
            $table->string('last_name', 100)->nullable();
            $table->string('last_name_en', 100)->nullable();
            $table->string('first_name', 100)->nullable();
            $table->string('first_name_en', 100)->nullable();
            $table->string('profile_img')->nullable();
            $table->string('facebook_url')->nullable();
            $table->string('twitter_url')->nullable();
            $table->string('linkedin_url')->nullable();
            $table->string('bio');
            $table->string('bio_en')->nullable();
            $table->date('dob');
            $table->integer('bank_id')->unsigned();
            $table->string('account_no', 15);
            $table->string('phone_number', 31);
            $table->string('email', 80)->unique();
            $table->string('password')->nullable();
            $table->rememberToken();
            $table->timestamps();

            $table->foreign('user_type_id')->references('id')->on('user_types')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
