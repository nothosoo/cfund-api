<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => 'auth:api'], function () {
    Route::post('logout', 'Auth\LoginController@logout');

    Route::get('/user', function (Request $request) {
        return $request->user();
    });

    Route::post('project/{id}', 'ProjectController@store');
    Route::put('project/{id}', 'ProjectController@update');
    Route::delete('project/{id}', 'ProjectController@destroy');

    Route::patch('settings/profile', 'Settings\ProfileController@update');
    Route::patch('settings/password', 'Settings\PasswordController@update');
});

// Route::group(['middleware' => 'guest:api'], function () {
    Route::post('login', 'Auth\LoginController@login');
    Route::post('register', 'Auth\RegisterController@register');

    Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail');
    Route::post('password/reset', 'Auth\ResetPasswordController@reset');

    Route::post('oauth/{provider}', 'Auth\OAuthController@redirectToProvider');
    Route::get('oauth/{provider}/callback', 'Auth\OAuthController@handleProviderCallback')->name('oauth.callback');
    Route::get('project', 'ProjectController@index')->name('project.index');
    Route::get('project/latest', 'ProjectController@latest');
    Route::get('project/finished', 'ProjectController@finished');
    Route::get('project/{id}', 'ProjectController@show');
    Route::get('user/projects/{id}', 'ProjectController@userProjects');
    Route::post('donation/{id}', 'DonationController@store');
    Route::get('category', 'CategoryController@index');
// });
